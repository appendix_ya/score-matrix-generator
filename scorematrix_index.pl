#!/usr/bin/perl -w

use RINGS::Web::HTML;
my $id = Get_Cookie_Info();

print "Content-type: text/html\n\n";

print <<EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="ja">
<head>
<META http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<META http-equiv="Content-Style-Type" content="text/css">
    <link rel="stylesheet" href="/rings2.css" type="text/css">

    <title>Score Matrix Tool</title>

<script Language="JavaScript"><!--
function change(obj,col,ft)
{
	obj.style.backgroundColor = col;
	obj.style.color = ft;
}
// --></script>


</head>
<Body><!-- alink="#ff0000" bgcolor="#ffffff" link="#0000ef" text="#000000" vlink="#55188a" -->
<div id = "menu">
<P class="glow"><font size=6><a href="/index.html"><img src="/rings4.png" border=0></a>Score Matrix Tool</font></P>
</div>
<hr>
<table border=0 cellpadding=0 cellspacing=0>
<tr valign="top">
<td width=200>
	<CENTER>
	<h4 style="text-align : center;" align="center"><A onmouseover="change(this,'#9966FF','#330066')" onmouseout="change(this,'#8484ee','#000033')" href="/index.html">Home</A></h4>
	<h4 style="text-align : center;" align="center"><A onmouseover="change(this,'#9966FF','#330066')" onmouseout="change(this,'#8484ee','#000033')" href="/help/help.html">Help</A></h4>
        <h4 style="text-align : center;" align="center">
EOF

my $bugTarget = "BugDisplay.pl";
if($id != 0){
   $bugTarget = "BugReportForm.pl?tool=Score Matrix Tool";
}

print <<EOF;
<a href="/cgi-bin/tools/Bug/$bugTarget" onMouseover="change(this,'#9966FF','#000033')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a></h4>

	</CENTER>
    
</td>
    <TD width="800" border="0" cellpadding="0" cellspacing="0" align ="center">
    <DIV align="center">

</DIV>
<p>
<FORM action="/cgi-bin/tools/ScoreMatrixTool/score_matrix.pl" method="POST" name="SCOMAT" enctype="multipart/form-data">
    <TABLE border= 3 cellpadding = 3 cellspacing = 0>
      <TBODY>
	<TR>
          <TD class = "bgc_glay"><B><FONT size="3">Matrix Name</FONT></B></TD>
          <TD><INPUT type="text" name="matrixname" value="Glycan_Score_Matrix(default_data_set)"></TD>
        </TR>
        <TR>
          <TD class = "bgc_glay" rowspan=2><div><FONT size="3"><B>Glycan data</B></FONT></div>
	  <div><FONT size="3">Direct input glycan data</FONT></div>
	  <div><FONT size="2">(KCF or KEGG Glycan ID)</FONT></div>
	  <!--<div><FONT size="3">In case using a data file,<br>the text area must be empty.</FONT></div>-->
	  </TD>
          <TD><textarea name ="GlycanData" rows="20" cols="40">
G00015
G00016
G00017
G00018
G00019
G00020
G00021
G00022
G00171
G00178
G00198
G00199
G00200
G00202
</textarea></TD>
        </TR>
        <TR>
          <!--<TD><INPUT type="file" name="file"></TD>-->
        </TR>
        <TR>
          <TD class = "bgc_glay"><B><FONT size="3">Glycan data type</FONT></B></TD>
          <TD><DIV><B><INPUT type="radio" name="type" value="0">KCF
	  <INPUT type="radio" name="type" value="1" checked>Glycan ID</B></DIV>
          <DIV><FONT size="2">Check the inputted glycan data type</FONT></DIV></TD>
        </TR>
        <TR>
          <TD class = "bgc_glay"><B><FONT size="3">Class dividing threshold</FONT></B></TD>
          <TD><input type="text"  name="Threshold" value="400" size="5" maxlength="5"></TD>
        </TR>
        <TR>
          <TD class = "bgc_glay"><B><FONT size="3">MCAW parameter</FONT></B></TD>
          <TD><DIV>Gap Penalty: <input type="text"  name="gap" value="-10" size="5" maxlength="5"></DIV>
          <DIV>Monosaccharide: <input type="text"  name="mono" value="60" size="5" maxlength="5"></DIV>
          <DIV>Anomer: <input type="text"  name="anomer" value="20" size="5" maxlength="5"></DIV>
          <DIV>Non reducing side carbon number: <input type="text"  name="nred" value="20" size="5" maxlength="5"></DIV>
          <DIV>Reducing side carbon number: <input type="text"  name="red" value="20" size="5" maxlength="5"></DIV></TD>
        </TR>
        <TR>
          <TD colspan="2" align="center">
          <input value="reset" onClick="SCOMAT.GlycanData.value='',SCOMAT.matrixname.value='' , SCOMAT.type.value='0'" type="reset"> <input value="run" type="submit"></TD>
        </TR>
      </TBODY>
    </TABLE>
    </FORM>
    </TD>
  </tr>
    </table>
</body>
</html>
EOF
