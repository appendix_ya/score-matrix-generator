#!/usr/bin/perl

use CGI;
use CGI::Carp qw/fatalsToBrowser/;
#BEGIN{
#   use CGI::Carp qw/carpout/;
#   open LOG, ">>", "/tmp/scorematrixcarp.log" or die("Cannot open carp log file");
#   carpout(LOG);
#}
#use rings;
use RINGS::Web::HTML;
use RINGS::Tool;
use RINGS::Web::user_admin;
use RINGS::Glycan::KCF;
use RINGS::Glycan::Mapping;
use Math::Round;
use File::Copy;
use File::Find;
#require 5.001;
require "../share/cgi-lib.pl";

my $dbh = Connect_To_Rings;
my $hostname = RINGS::Web::HTML::HOSTNAME;


##################
# Out put header #
##################
print "Content-type: text/html\n\n";
print "<html>\n";
print "<head>\n";
my $title = "Score Matrix Result";
my $head = Make_Head_From_Title($title);
print "$head\n";
print "</head>\n";
print "<body>\n";
my $h1 = Make_H1_From_Header($title);
print "$h1";
print "<br>\n";


###############
# Get User ID #
###############
my $id  = Get_Cookie_Info();

$time0 = time;		# Time-record


##################
# Make Directory #
##################
if($id != 0){
   if(!-d "/var/www/userdata/$id"){
      mkdir ("/var/www/userdata/$id") || die "failed: $!";
      mkdir ("/var/www/userdata/$id/ScoreMatrixTool") || die "failed: $!";
   }else{
      if(!-d "/var/www/userdata/$id/ScoreMatrixTool"){
         mkdir ("/var/www/userdata/$id/ScoreMatrixTool") || die "failed: $!";
      }
   }
}

if(!-d "/tmp/ScoreMatrixTool"){
   mkdir("/tmp/ScoreMatrixTool") || die "failed: $!";
}
if(!-d "/tmp/ScoreMatrixTool/$time0"){
   mkdir("/tmp/ScoreMatrixTool/$time0") || die "failed: $!";
}



#################
# Get form data #
#################
my @postdatas = get_post_data();
$dsname = $postdatas[0];
$GLYCAN = $postdatas[1];  #KCF data
$threshold = $postdatas[2];  #threshold to divide clusters
$gapPenalty = $postdatas[3];	  #MCAW parameter
$sugar = $postdatas[4];	  #MCAW parameter
$anomer = $postdatas[5];	  #MCAW parameter
$nreCarbon = $postdatas[6];	  #MCAW parameter
$reCarbon = $postdatas[7];	  #MCAW parameter

&create_sourceKCF_file;

#############################################################################################




#############################################################################################
#				calculate glycan score matrix			  	    #
#											    #
#  input: Matrix Name, KCF or Gid  data and Threshold					    #
#  output: two "monosaccharide and its linkage information"s and their score (high to low)  #
#############################################################################################

#################################################
#	read input data from the interface	#
#################################################

$kcf_data = "/tmp/ScoreMatrixTool/$time0/inputKCF.txt";	#KCF or Gid format

my %nodenum = ();
my $name;
my $count_id = 1;

open(KCF, $kcf_data);
while (<KCF>) {
    my $line = $_;
    if ($line =~ /ENTRY\s+(G99\S*)\s+Glycan/) {
       $name = $1;
       print "Error: Please do not use \"G99\" (or start from \"G99\") for ENTRY name. <BR>";
       print "     Please change the ENTRY name: $name . <BR>";
       exit;
    }elsif ($line =~ /ENTRY\s+([A-Za-z0-9_]+)\s+Glycan/) {
       $name = $1;
    }elsif ($line =~ /ENTRY\s+([^A-Za-z0-9]+)\s+Glycan/) {
       $line =~ s/[^A-Za-z0-9]/_/g;
       $line =~ /ENTRY\s+([A-Za-z0-9_]+)\s+Glycan/;
       $name = $1;
    }

    if ($line =~ /NODE\s+(\d+)/) {
        $nodenum{$name} = $1;
    }

}
close(KCF);


### Finish time for getting INPUT DATA 
$time1 = time;
$time_needed = $time1 - $time0;
open (TIME_FILE, "> /tmp/ScoreMatrixTool/$time0/time.txt");
   print TIME_FILE "Time for getting INPUT DATA = $time_needed\n";
close(TIME_FILE);





#####################################################
#	     Glycan alignment (all vs all)	    #
#						    #
#   KCaM alignmnet parametar			    #
#	db: database				    #
#	3: local alignment, 4: global alignmnet	    #
#	kcf file				    #
#	none (candidate file)			    #
#	0: similarity, 1: match			    #
#####################################################

#print "kcf_data => $kcf_data<BR>";

#my $pre_kcam_data = `/var/www/opt/jre1.6.0_05/bin/java -jar /opt/glycomedb/KCaM.jar db 3 $kcf_data none 0`;	#similarity
my $pre_kcam_data = `/var/www/opt/jre1.6.0_05/bin/java -jar /opt/glycomedb/KCaM.jar db 3 $kcf_data none 1`;	#match

my $kcam_check = length($pre_kcam_data);

#print "<BR> kcam_check => $kcam_check<BR>";

if($kcam_check == 0){
   &printHeader("ERROR: We could not finish a KCaM calculation.<BR> Please contact us via RINGS Bug Report.\n");
   exit;
}

### Finish time for kcam calculation 
$time2 = time;
$time_needed = $time2 - $time1;
open (TIME_FILE, ">> /tmp/ScoreMatrixTool/$time0/time.txt");
   print TIME_FILE "Time for KCaM calculation = $time_needed\n";
close(TIME_FILE);


my $kcam_data = '';
my @tmp_line = [];

my @tmp_line  = split(/\n/,$pre_kcam_data);

open(FILE, "> /tmp/ScoreMatrixTool/$time0/kcam.txt");
foreach my $tmp (@tmp_line){
    if($tmp =~ /^([A-Za-z0-9_]+\s+[A-Za-z0-9_]+\s\d+\.\d+)\s+.+$/){
       my $tmp_clean = $1;
       print FILE "$tmp_clean\n";
    }
}
close(FILE);







#########################################################################
#	            Clustering (Fitch-Margoliash Method)		#
#									#
#   input: KCaM result file and threshold value (def: 400)		#
#   output: cluster file(s) as a text file 				#
#           And an array (@profile_name) including above file name(s)	#
#########################################################################
#system "perl matrix_gen.pl kcam_$kcfTime.txt $threshold";


#################################
#      making first matrix	#
#################################
$kcam_data = "/tmp/ScoreMatrixTool/$time0/kcam.txt";
my ($minI, $minJ, $minD, $checkAr, $matAr) = &ReadArray($kcam_data);

$len = @{$checkAr};
$extra = 0;

open(MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
   print MSG "The number of glycan IDs in checkAr => $len\n\n and the entries are:\n";
   $" = ',';
   print MSG "@{$checkAr}\n\n";
close(MSG);

### adding out group ###
if (@{$checkAr}[$len-1] =~/^G99/){
   $extra = 1;
}
if ($extra == 0){ 		#add outer group into KCaM File
   ($len, $checkAr, $matAr) = &G99_into_KCaM_FILE($len, \@{$checkAr}, \@{$matAr}, $kcam_data);
}





#####################################################################################
#			   Fitch-Margoliash calculation				    #
#										    #
# $threshold: divide into two clusters when a distance is longer than the threshold #
# $kcam_data: KCaM result file including out group				    #
# $len: the sum of the number of glycans and clusters (the length of \@{$checkAr})  #
# $minI(J): a glycan or a cluster with has the shortest distance ($minD)	    #
# \@{$matAr}: distance matrix (global)						    #
#####################################################################################

($ItemCountScore, @Clust) = &fitch_margoliash($threshold,$kcam_data, $len, $minI,$minJ,$minD,\@{$matAr},\@{$checkAr}); #calculation of Fitch-Margoliash

@profile_name = ();
@profile_name = &make_clust_FILE(\%{$ItemCountScore}, @Clust); #output the clustering result



### Finish time for clustering
$time1 = time;
$time_needed = $time1 - $time2;
open (TIME_FILE, ">> /tmp/ScoreMatrixTool/$time0/time.txt");
   print TIME_FILE "Time for clustering = $time_needed\n";
close(TIME_FILE);






#################################################
#		   MCAW program			#
#						#
# get multiple glycan alignment			#
# input: cluster file(s) (@profile_name)	#
#	 the first kcf file $kcf_data		#
# output: MCAW result				#
#################################################

@file_check = ();
my $check_NumFile = -1;


open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
   print MSG "\n\n\n\n\nMCAW param => \n";
   print MSG "\tgapPenalty = $gapPenalty \n";
   print MSG "\tsugar value = $sugar\n";
   print MSG "\tanomer value = $anomer\n";
   print MSG "\tnon-reducing Carbon bond value = $nreCarbon\n";
   print MSG "\treducing Carbon bond value = $reCarbon\n\n";
close(MSG);


foreach my $tmp (@profile_name){
   $tmp =~ s/\,/-/g;
   if($tmp =~ /-/){
      $class_file = "/tmp/ScoreMatrixTool/$time0/$tmp.txt";
      $pkcf = `/var/www/opt/jre1.5.0_09/bin/java -classpath /var/www/java/MCAW.jar MCAW.Main $class_file $kcf_data $gapPenalty $sugar $anomer $nreCarbon $reCarbon`;

      open (PKCF, "> /tmp/ScoreMatrixTool/$time0/pkcf-$tmp.txt");
	 print PKCF $pkcf;
      close (PKCF);
      $check_NumFile = 1;

      open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
	  print MSG "\n\n\nIDs for MCAW calculation: $tmp\n\n";
      close(MSG);


   }else{
      push(@file_check, $tmp);
   }
}

if($check_NumFile == -1){
   &printHeader("ERROR: No Cluster(s)\n Threshold may be too small or the number of input glycan data were not enough.\n");
   exit;
}

### Finish time for MCAW 
$time2 = time;
$time_needed = $time2 - $time1;
open (TIME_FILE, ">> /tmp/ScoreMatrixTool/$time0/time.txt");
   print TIME_FILE "Time for MCAW = $time_needed\n";
close(TIME_FILE);






#################################################
#		score calculation		#
#################################################

@matF = ();
@matG = ();
@matE = ();
@P = ();
%result_Hash;

for(my $i = 0; $i < 1000; $i++){          #$i = the size of matF
   for(my $j = 0; $j <= $i ; $j++){
      $matF[$i][$j] = 0;
   }
}
for(my $i = 0; $i < 1000; $i++){          #$i = the size of matG
   for(my $j = 0; $j <= $i ; $j++){
      $matG[$i][$j] = '';
   }
}
&Copy_Arrays(\@matG,\@matE);

($matF, $allLinkAr, $linkTotal, $tmp_missing_hash) = &Get_matF(\@profile_name, \@matF, $time0);
%missing_entries = %{$tmp_missing_hash};




open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
   $" = ',';  
   print MSG "\n\n============= TOTAL matF =============\n";

   my $size_links = @{$allLinkAr};
   for(my $i = 0; $i < $size_links; $i++){
      print MSG "@{$allLinkAr}[$i]\n"; 
   }

   for(my $i = 0; $i < $size_links; $i++){
      for(my $j = 0; $j <= $i ; $j++){
         if(@{$matF[$j]}[$i] != ''){
            $matG[$j][$i] = @{$matF[$j]}[$i]/$linkTotal;
         }
         print MSG "@{$matF[$j]}[$i], "; 
      }
      print MSG "\n";
   }

   print MSG "\n\n================= matG ===================\n";
   for(my $i = 0; $i < $size_links; $i++){
      my $p_ij = 0;
      for(my $j = 0; $j < $size_links; $j++){
         if($i != $j && $matG[$j][$i] != ''){
            $p_ij += $p_ij+$matG[$j][$i];
         }elsif($i != $j && $matG[$i][$j] != ''){
            $p_ij += $p_ij+$matG[$i][$j];
         }
         print MSG "@{$matG[$j]}[$i], "; 
      }
      $P[$i] = $matG[$i][$i]+$p_ij/2;
      print MSG "\n";
   }

   print MSG "\n\n================== P ===================\n";
   for(my $i = 0; $i < $size_links; $i++){
      for(my $j = 0; $j <= $i ; $j++){
         if($i == $j && $matG[$j][$i] != ''){
            $matE[$j][$i] = $P[$i]*$P[$i];
         }elsif($i != $j && $matG[$j][$i] != ''){
            $matE[$j][$i] = 2*$P[$i]*$P[$j];
         }
         print MSG "@{$matE[$j]}[$i], "; 
      }
      print MSG "\n";
   }

   print MSG "\n\n================== matE ==================\n";
   for(my $i = 0; $i < $size_links; $i++){
      for(my $j = 0; $j <= $i ; $j++){
         if($matE[$j][$i] != '' && $matG[$j][$i] != ''){
            $matF[$j][$i] = round(log($matG[$j][$i] / $matE[$j][$i]) / log(2));
	    my $key = $j."-".$i;
	    $result_Hash{$key} = $matF[$j][$i];
         }
         print MSG "@{$matF[$j]}[$i], "; 
      }
      print MSG "\n";
   }
close(MSG);

### Finish time for score calculation 
$time1 = time;
$time_needed = $time1 - $time2;
open (TIME_FILE, ">> /tmp/ScoreMatrixTool/$time0/time.txt");
   print TIME_FILE "Time for score calculation = $time_needed\n";
close(TIME_FILE);


open (RESULT, "> /tmp/ScoreMatrixTool/$time0/result.txt");
open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
@keysAr = sort {
   $result_Hash{$b} <=> $result_Hash{$a};
} keys %result_Hash;


foreach my $key (@keysAr){
   my @tmp_keys = split(/\-/,$key);
   print RESULT "@{$allLinkAr}[$tmp_keys[0]] - @{$allLinkAr}[$tmp_keys[1]] = $result_Hash{$key}\n";
}




my $size_FC = @file_check;
print MSG "the number of non-clusters ID(s) => $size_FC\n";
print MSG "--------------- non-clusters ID(s) (below) --------------- \n";

if($size_FC != 0){
   print MSG "\nThese are clustered as alone => not calculated in score calculation\n";
   for(my $i=0; $i<$size_FC-1; $i++){
      print MSG "$file_check[$i]\n";
   }
}

close(RESULT);
close(MSG);



##### Output ############
print <<EOF;
<div id="doc">
   <table width = 1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign = "top">
      <td width = 70>
         <h4 style = "text-align : center;" align = "center">
            <a href = "index.html" onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">Home</a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/help/help.html" onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">Help</a>
         </h4>
         <h4 style = "text-align : center;" align = "center">
            <a href = "/cgi-bin/tools/Bug/BugReportForm.pl?tool = Score Matrix Generator Tool"
            onMouseover = "change(this, '#9966FF', '#330066')" on Mouseout = "change(this, '#8484ee', '#000033')">Feedback</a>
         </h4>
      </td>
   <td align = "center" width = 500><Font size = 4><B>
   <TABLE border = 4 cellpadding = 4 cellspacing = 0>
   <TBODY >
      <TR>
         <TD width=100 class = "bgc_glay"><B><FONT size = "3"><center>No.</center></FONT></B></TD>
         <TD width=140 class = "bgc_glay"><B><FONT size = "3"><center>Link 1</center></FONT></B></TD>
         <TD width=140 class = "bgc_glay"><B><FONT size = "3"><center>Link 2</center></FONT></B></TD>
         <TD width=100 class = "bgc_glay"><B><FONT size = "3"><center>Score</center></FONT></B></TD>
      </TR>
</div>
EOF


# =================================
my $count = 1;
my $link1 = '';
my $link2 = '';
my $score = 0;

my %missing_ar = ();

foreach my $key (@keysAr){
   my @tmp_keys = split(/\-/,$key);
   $link1 = @{$allLinkAr}[$tmp_keys[0]];
   $link2 = @{$allLinkAr}[$tmp_keys[1]];
   $score = $result_Hash{$key};

   my @tmp = ($link1,$link2,$score,$count);
   $missing_ar{$count} = [@tmp];

print <<EOF;
   <TR>
      <TD><center>$count</center></TD>
      <TD><center>$link1</center></TD>
      <TD><center>$link2</center></TD>
      <TD><center>$score</center></TD>
   </TR>
EOF

   $count++;
}

print <<EOF;
   </TBODY></TABLE></td></tr></table>
EOF



$count = $count-1;
my $comment = "<p><td width=400><Font size=3><BR>";
my $note = "<td width=400><Font size=3><B>";
$note .= "(NOTE) Below link(s) are candidate missing data.<BR><BR>";

$note .= "<TABLE border=2 cellpadding=1 cellspacing=0><TBODY><TR>";
$note .= "<TD width=80><B><FONT size=\"2\"><center>Result No.</center></FONT></B></TD>";
$note .= "<TD width=120><B><FONT size=\"2\"><center>Link 1</center></FONT></B></TD>";
$note .= "<TD width=120><B><FONT size=\"2\"><center>Link 2</center></FONT></B></TD>";
$note .= "<TD width=80><B><FONT size=\"2\"><center>Score</center></FONT></B></TD></TR></div>";

my $tcheck = 0;

for(my $tk1=1; $tk1<$count; $tk1++){
   my $tl1 = $missing_ar{$tk1}[0];
   my $tl2 = $missing_ar{$tk1}[1];

   for(my $tk2=$tk1+1; $tk2<$count+1; $tk2++){
      my $tl3 = $missing_ar{$tk2}[0];
      my $tl4 = $missing_ar{$tk2}[1];

      my $tcheck1 = &GetMissing($tl1,$tl2,$tl3,$tl4);
      if($tcheck1 == 1){
	 my $id1 = $missing_ar{$tk1}[0];
	 my $id2 = $missing_ar{$tk1}[1];
	 my $id3 = $missing_ar{$tk2}[0];
	 my $id4 = $missing_ar{$tk2}[1];
	 my @id_array1 = @{$missing_entries{$id1}};
	 my @id_array2 = @{$missing_entries{$id2}};
	 my @id_array3 = @{$missing_entries{$id3}};
	 my @id_array4 = @{$missing_entries{$id4}};


	 $note .= "<TR><TD><center>No.$tk1</center></TD>";
	 $note .= "<TD><center>$missing_ar{$tk1}[0]</center></TD>";
	 $note .= "<TD><center>$missing_ar{$tk1}[1]</center></TD>";
	 $note .= "<TD><center>$missing_ar{$tk1}[2]</center></TD></TR>";

	 $note .= "<TR><TD><center>No.$tk2</center></TD>";
	 $note .= "<TD><center>$missing_ar{$tk2}[0]</center></TD>";
	 $note .= "<TD><center>$missing_ar{$tk2}[1]</center></TD>";
	 $note .= "<TD><center>$missing_ar{$tk2}[2]</center></TD></TR>";


         if(@id_array1 > 0){
  	    $comment .= "Link($id1) is contained at @id_array1,<BR>";
         }
         if(@id_array2 > 0){
  	    $comment .= "Link($id2) is contained at @id_array2,<BR>";
         }
         if(@id_array3 > 0){
  	    $comment .= "Link($id3) is contained at @id_array3,<BR>";
         }
         if(@id_array4 > 0){
  	    $comment .= "Link($id4) is contained at @id_array4,<BR>";
         }
	    
         $tcheck += $tcheck1;
      }
   }
}
$note .= "</TBODY></TABLE></td></tr></table>";
$comment .= "<td>";


if($tcheck > 0){
print <<EOF;
   <div align = "center">
EOF
   print "<BR><BR>----------------------------------------------------------------------------";
   print "----------------------------------------------------------------------------<BR>";
   print $note;
   print $comment;
print <<EOF;
   </div>
EOF

   open (RESULT, ">> /tmp/ScoreMatrixTool/$time0/result.txt");
      print RESULT $note;
      print RESULT $comment;
   close(RESULT);

}
print <<EOF;
   </body></html>
EOF


### store data in database
if($id != 0){
   #Get input data size
   my $input_data_size = -s $kcf_data;

   #Get output data size
   $from_result = "/tmp/ScoreMatrixTool/$time0/result.txt";
   $from_msg = "/tmp/ScoreMatrixTool/$time0/msg.txt";

   my $output_data_size = -s $from_result;

   my $Output_id = Insert_ScoreMatrix_Data($dbh, $dsname, $threshold, $input_data_size, $output_data_size, $id);

   if ($Output_id < 0) {
	print "ERROR storing data: $Output_id<p>";
   }

   #$to_result = "/var/www/userdata/$id/ScoreMatrixTool/result_$time0.txt";
   
   Make_Dir("/var/www/userdata/$id");
   Make_Dir("/var/www/userdata/$id/ScoreMatrixTool");
   Make_Dir("/var/www/userdata/$id/ScoreMatrixTool/$Output_id");
   $to_result = "/var/www/userdata/$id/ScoreMatrixTool/$Output_id/result.txt";
   $to_msg = "/var/www/userdata/$id/ScoreMatrixTool/$Output_id/msg.txt";

   copy($from_result, $to_result);
   copy($from_msg , $to_msg);

   move($kcf_data, "/var/www/userdata/$id/ScoreMatrixTool/$Output_id/input.txt");
}










#############################################################
# ######################################################### #
# #                     subroutin                         # #
# ######################################################### #
#############################################################

#################################################################
#               ReadArray               			#
# input: kcam file						#
# output: 							#
# 	$minD: minimum distance 				#
#       $minI and $minJ:  index i and j which have $minD 	#
#	@checkAr:  complete range of names of Glycan Entry 	#
#       @matAr: the first square matrix 	 		#
#	        rows = @checkAr and columns =  @checkAr,	#
#	        and their values are 				#
#		100000 for diagonally values 			#
#		KCaM distance score for other values		#
#################################################################
sub ReadArray
{
 my $file = shift @_;	#KCaM result
 my $minD = 10000000;	#to find minimum distance, the first minD is set as large value
 my @checkAr = ();	#complete range of Glycan entry
 my @matAr = ();	#the first matrix (@checkAr * @checkAr, and value = 100000)

 open(K_DATA, "<$file");
 while (<K_DATA>){
    $line = $_;  
       @lineAr = split(/\s+/,$line);
       $g_id_1 = $lineAr[0];
       $g_id_2 = $lineAr[1];

       $I = &Put1000000ToMyself($g_id_1, \@checkAr, \@matAr);
       $J = &Put1000000ToMyself($g_id_2, \@checkAr, \@matAr);

       if($g_id_1 =~ /G9/){
          $d = $lineAr[2];
       }else{
          $node1 = $nodenum{$g_id_1};
          $node2 = $nodenum{$g_id_2};

          if($node1 >= $node2){
             my $max = $node1*100;
             $d = $max-$lineAr[2];
          }elsif($node2 > $node1){
             my $max = $node2*100;
             $d = $max-$lineAr[2];
          }else{
             print "ERROR!!: the size of $lineAr[0] or $lineAr[1] is not available!\n";
  	     exit;
          }
       }

       (@matAr) = &PutScoreToMat($I,$J,$d,@matAr);

       if ($minD > $d){
          $minD = $d;
          $minI = $I;
          $minJ = $J;
       }
 }
 close(K_DATA);
 return ($minI, $minJ, $minD,\@checkAr, \@matAr);
}



#########################################
#            Put1000000ToMyself         #
#########################################
sub Put1000000ToMyself
{
 my ($col, $checkAr, $matAr) = @_;
 $the_size_of_checkAr = @{$checkAr};

 my $x = &aindex ($checkAr,$col);

 if ($x == -1){
    $x = @{ $checkAr };
    my @newAr;
    $newAr[$x] = 1000000;
    push(@{ $checkAr },$col);
    @{ $matAr }[$x] = [ @newAr ];
 }
 return $x;
}




#########################################
#               PutScoreToMat           #
#########################################
sub PutScoreToMat
{
   my ($I, $J, $d, @matAr) = @_;
   my $length1 = @{$matAr[$I]};
   my $length2 = @{$matAr[$J]};

   if ($J >= $length1){
      my $plusX = $J-($length1);
      for(0..($plusX)){
         push(@{$matAr[$I]},'');
      }
   }
   if ($I >= $length2){
      my $plusY = $I-($length1);
      for(0..($plusY)){
         push(@{$matAr[$J]},'');
      }
   }

   $matAr[$I][$J]=$d;
   $matAr[$J][$I]=$d;
   return @matAr;
}





#################################################################
#            Not existing G99 (outer group) in KCaM file        #
#               add G99 into the end of KCaM file               #
#################################################################
sub G99_into_KCaM_FILE
{
 my ($len, $checkAr, $matAr, $kcam_file) = @_;

 push(@{$checkAr},"G99");

 my @tend = ();
 $len = $len+1;
 for (my $x=0; $x < $len-1; $x++){
    my $d = 100000;
    push (@tend, $d);

    open(FILE, ">>$kcam_file");
       print FILE "G99\t@{$checkAr}[$x]\t$d\n";
    close(FILE);
 }
 for (my $i=0; $i < $len-1; $i++) {
    push (@{${$matAr}[$i]}, $tend[$i]);
 }
 push (@tend, 1000000);
 push (@{$matAr},[@tend]);

 return ($len, $checkAr, $matAr);
}



#####################################################################
#  the number of items, two items which has the minimum distance,   #
#  the minimum distance, distance matrix and array of items         #
#    (($len, $minI, $minJ, $minD, $matAr, $checkAr)                 #
#                                                                   #
#  Clustering based on Fitch-Margoliash method using these items    #
#####################################################################
sub fitch_margoliash
{
 my ($threshold,$file_kcam, $the_length, $min_I, $min_J, $min_D, $matrix_Ar, $check_Ar) = @_;
 my $threshDIS = $threshold;

 @Clust = ();
 @Item = ();
 @Dis = ();
 %ItemCountScore = ();

 $count_check = 0;

 while ($the_length > 1) {

    $count_check += 1;

    my $maxDis = 0;
    $clustDis = $min_D;
    $item1 = @{$check_Ar}[$min_I];
    $item2 = @{$check_Ar}[$min_J];
    $addITEM1 = @{$check_Ar}[$min_I];
    $addITEM2 = @{$check_Ar}[$min_J];

    $item1 =~ s/[\(\)]//g;
    $item2 =~ s/[\(\)]//g;
    $item1 =~ s/\,+/,/g;
    $item2 =~ s/\,+/,/g;

    if ($item1 =~ /\,/){
       $maxDis1 = &get_Max($file_kcam, $item1);
    }else{
       $maxDis1 = 0;
    }

    if ($item2 =~ /\,/){
       $maxDis2 = &get_Max($file_kcam, $item2);
    }else{
       $maxDis2 = 0;
    }

    if ($item1 !~ /\,/ && $item2 !~ /\,/){
       $maxDis = 0;
    }

    if ($maxDis1 >= $maxDis2){
       $maxDis = $maxDis1;
    }else{
       $maxDis = $maxDis2;
    }

    if ($maxDis > 0){
       $ratio = $clustDis/$maxDis;
    }else{
       $ratio = 0;
    }

    open(MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
       print MSG "\n\n\n***** Time of calculation => $count_check \n";
       print MSG "---- <Before copyMatAr subroutine> ----\n";
       print MSG "\tmin_I => $min_I(${$check_Ar}[$min_I]),\n\tmin_J => $min_J(${$check_Ar}[$min_J]),\n\tmin_D => $min_D\n";

    ($matrix_Ar, $check_Ar, $min_I, $min_J, $min_D, $ItemCountScore) = &CopyMatAr(\%ItemCountScore,$the_length,$min_I,$min_J,$min_D,\@{$matrix_Ar},\@{$check_Ar});

       print MSG "---- <After copyMatAr subroutine> ----\n";
       print MSG "\tmin_I => $min_I(${$check_Ar}[$min_I]),\n\tmin_J => $min_J(${$check_Ar}[$min_J]),\n\tmin_D => $min_D\n";
       $" = ',';
       print MSG "\ncheckAr => @{$checkAr}\n\n";
    close(MSG);

    if ($clustDis >= $threshDIS){
       @Clust = &addClust($addITEM1, @Clust);
       @Clust = &addClust($addITEM2, @Clust);
       my $sizeC = @Clust;
    }
    if ($clustDis == 2){
       @Clust = &addClust($addITEM1, @Clust);
       @Clust = &addClust($addITEM2, @Clust);
       my $sizeC = @Clust;
    }
    $the_length = @{$check_Ar};
 }
 return (\%{$ItemCountScore}, @Clust);
}





#########################################
#               get_Max                 #
#########################################
sub get_Max
{
  my ($file, $clust) = @_;
  my @clustPart = split(/[\(\)\,]/,$clust);
  $pSize = @clustPart;
  my $maxDis = 0;

  for (my $i=0; $i < $pSize-1; $i++){
     for (my $j=$i+1; $j < $pSize; $j++){
        if ($clustPart[$i] =~ /\d+/ && $clustPart[$j] =~ /\d+/){
           $parta = "G".$clustPart[$i];
           $partb = "G".$clustPart[$j];
           open(FILE,$file);
              while(<FILE>){
                 my $line = $_;
                 if ($line =~/^$parta\s+$partb\s+(\d+\.\d)/){
                    $Kdis = $1;
                 }elsif ($line =~/^$partb\s+$parta\s+(\d+\.\d)/){
                    $Kdis = $1;
                 }
              }
           close(FILE);

           if($parta =~ /G9/){
              $nodeA = 0;
              $nodeB = $nodenum{$partb};
           }elsif($partb =~ /G9/){
              $nodeA = $nodenum{$parta};
              $nodeB = 0;
           }else{
              $nodeA = $nodenum{$parta};
              $nodeB = $nodenum{$partb};
           }

           if($nodeA >= $nodeB){
              my $max = $nodeA*100;
              $d = $max-$Kdis;
           }elsif($nodeB > $nodeA){
              my $max = $nodeB*100;
              $d = $max-$Kdis;
           }

           if($maxDis < $d){
              $maxDis = $d;
           }
        }
     }
  }
  return ($maxDis);
}



#########################################################################################
#                                        CopyMatAr                                      #
#            convine "minI" and "minJ" which has "minD", and add into @checkAr          #
#   Then re-calculate distance matrix, @matAr, and find new "minI", "minJ", and "minD"  #
#########################################################################################

sub CopyMatAr
{
###################### re-calculate distance matrix @matAr #########################
 my ($ICS, $size, $I,$J,$D,$matAr,$checkAr) = @_;
 my @fAr =();
 my @matAr2 = ();
 my @checkAr2 = ();
 my $sum1 = -1000000;
 my $sum2 = -1000000;
# my $sum1 = 0;
# my $sum2 = 0;
 my $mean = 0;

open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
print MSG "\n-------------- Error check -------------- \n";
 for ($i = 0; $i<$size; $i++){                  #Re-calculate the matAr
    my @newAr = ();
    my $ijSum = 0;

    for ($j = 0; $j< $size; $j++){
       if ($i ne $I && $i ne $J && $j ne $I && $j ne $J){
          push(@newAr,@{@{$matAr}[$i]}[$j]);
       }elsif (@{@{$matAr}[$i]}[$j] ne '') {
          $ijSum += @{@{$matAr}[$i]}[$j];
          if ($i eq $I && $j ne $J){
             $sum1 += @{@{$matAr}[$i]}[$j];
print MSG "if(1) sum1 => $sum1\n";

          }elsif ($i eq $J && $j ne $I){
             $sum2 += @{@{$matAr}[$i]}[$j];
print MSG "if(2) sum2 => $sum2\n";

          }
       }
    }

    if ($ijSum == 0) {
       push(@newAr, '');
    }else{
       push(@newAr, $ijSum/2);
    }

    if ($i ne $I && $i ne $J) {
       push(@checkAr2,@{$checkAr}[$i]);
       if ($ijSum == 0) {
          push(@fAr, '');
       }else{
          push(@fAr, $ijSum/2);
       }
       push(@matAr2, [@newAr]);
    }
 }


############# calculate the length of branch "minI" and "minJ", and store into "%ItemCountScore"  ############################
 push(@fAr,1000000);
 push(@matAr2, [@fAr]);

 my @new = ();
 if ($size>2){
    $mean1 = $sum1/($size-2);
    $mean2 = $sum2/($size-2);
    $checkPLUS = $mean1+$mean2;
    $checkMINUS = abs($mean1-$mean2);

print MSG "\nl.877) checkPLUS = $checkPLUS and checkMINUS = $checkMINUS\n\n";
print MSG "minI = $I ($fAr[$I]) and minJ = $J ($fAr[$J])\n";
print MSG "size = $size\n";
print MSG "sum1 = $sum1 and sum2 = $sum2\n";
print MSG "mean1 = $mean1 and mean2 = $mean\n";
print MSG "checkPLUS = $checkPLUS and checkMINUS = $checkMINUS\n";
print MSG "check Array (index I($I)) => ${$checkAr}[$I]\ncheck Array (index J($J)) => ${$checkAr}[$J]\n";

    if ($checkMINUS > $checkPLUS){
       print MSG "\n\nERROR(1) checkPLUS < checkMINUS\n";
       print "<BR>ERROR(1) checkPLUS < checkMINUS<BR>";
       close(MSG);
       exit;
    }

    close(MSG);

    $DisA = ($D+($mean1-$mean2))/2;
    $DisB = ($D+($mean2-$mean1))/2;

    if ($DisA < 0){
       $DisA = 1;
    }
    if ($DisB < 0){
       $DisB = 1;
    }

    if (${$checkAr}[$I]=~/^[A-Za-z0-9_]+$/ && ${$checkAr}[$J]=~/^[A-Za-z0-9_]+$/){
       $parta = ${$checkAr}[$I];
       $partb = ${$checkAr}[$J];

       @new = (1, $DisA, $DisA);
       for ($i = 0; $i<3; $i++){
          $value = $new[$i];
          push(@{$ICS{$parta}},$value);
       }
       @new = (1, $DisB, $DisB);
       for ($i = 0; $i<3; $i++){
          $value = $new[$i];
          push(@{$ICS{$partb}},$value);
       }

       $combined = "\(".@{$checkAr}[$I].",".@{$checkAr}[$J]."\)";
       push(@checkAr2, $combined);
       push(@Item, (@{$checkAr}[$I],@{$checkAr}[$J],$combined));
       push(@Dis, ($DisA,$DisB,0));

    }elsif (@{$checkAr}[$I]=~/^[A-Za-z0-9_]+$/ && @{$checkAr}[$J]=~/\([A-Za-z0-9_]+.*,[A-Za-z0-9_]+.*\)/){
       $parta = @{$checkAr}[$I];
       $partb = @{$checkAr}[$J];
       ($distance, $ICS) = &get_NormDis(\%ICS,$partb,$DisB);
       @new = (1, $DisA, $DisA);
       for ($i = 0; $i<3; $i++){
          $value = $new[$i];
          push(@{$ICS{$parta}},$value);
       }

       $combined = "\(".@{$checkAr}[$I].",".@{$checkAr}[$J]."\)";
       push(@checkAr2, $combined);
       push(@Item, (@{$checkAr}[$I],$combined));
       push(@Dis, ($DisA,$distance));

    }elsif (@{$checkAr}[$J]=~/^[A-Za-z0-9_]+$/ && @{$checkAr}[$I]=~/\([A-Za-z0-9_]+.*,[A-Za-z0-9_]+.*\)/){
       $parta = @{$checkAr}[$J];
       $partb = @{$checkAr}[$I];
       ($distance, $ICS) = &get_NormDis(\%ICS,$partb,$DisB);
       @new = (1, $DisA, $DisA);
       for ($i = 0; $i<3; $i++){
          $value = $new[$i];
          push(@{$ICS{$parta}},$value);
       }

       $combined = "\(".@{$checkAr}[$J].",".@{$checkAr}[$I]."\)";
       push(@checkAr2, $combined);
       push(@Item, (@{$checkAr}[$J],$combined));
       push(@Dis, ($DisA,$distance));

    }elsif (@{$checkAr}[$I]=~/[A-Za-z0-9_]+.*,.*([A-Za-z0-9_]+)\)/ && @{$checkAr}[$J]=~/\(([A-Za-z0-9_]+).*,[A-Za-z0-9_]+.*/){
       $parta = @{$checkAr}[$I];
       $partb = @{$checkAr}[$J];
       ($x, $ICS) = &get_NormDis(\%ICS ,$parta,$DisA);
       ($y, $ICS) = &get_NormDis(\%ICS,$partb,$DisB);

       $combined = "\(".@{$checkAr}[$I].",".@{$checkAr}[$J]."\)";
       push(@checkAr2, $combined);
       push(@Item, $combined);
       push(@Dis, "[$x,$y]");

    }else{
       print "ERROR!!: no matched pattern of glycan subgroup\n";
       exit;
    }
 }


#################### find minimum-d(minD) ######################
 my $size_of_matAr2 = @matAr2;
 $minD = 1000000;
 for (my $i=0; $i<$size_of_matAr2; $i++){
    for (my $j=0; $j<$size_of_matAr2; $j++){
       if ($matAr2[$i][$j] ne '' && $minD > $matAr2[$i][$j]){
          $minD = $matAr2[$i][$j];
          $minI = $i;
          $minJ = $j;
       }
    }
 }

 if ($minD > 1000000 && $size > 1){
    print "Error: the minimum value ($minD) is > 1000000 although the size = $size !!\n";
    exit;
 }

 return (\@matAr2, \@checkAr2, $minI, $minJ, $minD, \%{ICS});
}




#################################################
#              get_NormDis                      #
#################################################
sub get_NormDis
{
  my ($ICS, $glycan1,$Dis) = @_;
  my $score = 0;
  @parts = split(/[\(\)\,]/,$glycan1);

  $partsSize = @parts;
  @parts2 = ();
  for (my $item=0; $item<$partsSize; $item++){
     $score = $score + $ICS{$parts[$item]}[2];
  }
  $distance = $Dis-$score/$partsSize;

  if ($distance < 0){
     $distance = 1;
  }

  ($distance, $ItemCountScore) = &addScore($partsSize, $distance, \@parts, \%{$ICS});

  return ($distance, \%{$ItemCountScore});
}




#########################################
#              addScore                 #
#########################################
sub addScore
{
  my ($pSize, $addSco, $gids2, $ICS) = @_;
  my @gids = @{$gids2};

  $gSize = @gids;

  for (my $item=0; $item<$gSize; $item++){
     if ($gids[$item] =~ /[A-Za-z0-9_]+/){
        $gid = $gids[$item];
        $ICS{$gid}[0] = $ICS{$gid}[0]+1;
        $normSc = $addSco/$ICS{$gid}[0];
        $ICS{$gid}[1] = $ICS{$gid}[1]+$normSc;
        $ICS{$gid}[2] = $ICS{$gid}[2]+$addSco/$pSize;
     }
  }

  return ($addSco, \%{$ICS});
}




##################################################################
#                           addClust                    	 #
#    save a cluster into @clust when the length of the branch 	 #
#                is longer than the threshold         	         #
##################################################################
sub addClust
{
  my ($entry, @Clust) = @_;
  my $x = -1;
  my $sizeC = @Clust;

  $entry =~ s/\(//g;
  $entry =~ s/\)/\+/g;

  $plus = index($entry,'+');

  if ($sizeC == 0){
     push(@Clust, $entry);
  }elsif ($entry =~ /[a-zA-Z0-9_]+/ && $plus == -1){
     push(@Clust, $entry);
  }else{
     my $index = -10;

     for (my $i=0; $i<$sizeC; $i++){
        my $item = $Clust[$i];

        if (defined $item){
           $size = length($item);
        }else{
           $size = 0;
        }

        my $length = length($entry);

        if ($length > 1 && $size > 1){
           $index = index($entry, $item);
           if ($index != -1){
              substr($entry, $index, $size, '');
              if($entry =~ /^\,([a-zA-Z0-9_]+.*)$/){
                 $entry = $1;
              }elsif($entry =~ /\,\,/){
                 $entry =~ s/\,\,/,/g;
              }
              $x = 1;

              if ($entry =~ /^[a-zA-Z0-9_]+\+([a-zA-Z0-9_]+.+)$/){
                 $entry = $1;
              }
           }
        }
     }
     if ($x == 1){
        $entry =~ /^([a-zA-Z0-9_]+.*)\+$/;
        $entry = $1;
     }
     if ($entry ne '' and $index => -1){
        push(@Clust, $entry);
     }
  }
  return (@Clust);
}




#################################################
#  output the result by files                   #
#  each file contain a cluster                  #
#  The name of a file is hyphenated G-ids       #
#################################################
sub make_clust_FILE
{
 my ($ItemCountScore, @Clust) = @_;
 my $length = @Clust;
# my $mode = 0;
 my @profile_name = ();

 for(my $aa = 0; $aa <= $length; $aa++){
    my $aCluster = $Clust[$aa];
    if (defined $aCluster && $aCluster =~ /^([a-zA-Z0-9_]+,)([a-zA-Z0-9_]+.+)$/){
       $aCluster =~ s/\+//g;
       push (@profile_name, $aCluster);

       my @clustAr = split(/\,/,$aCluster);
       my $clustArNum = @clustAr;
       my $filename = "/tmp/ScoreMatrixTool/$time0/".$aCluster.".txt";
       $filename =~ s/\,/-/g;

       open(OUT, "> $filename");
       open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
       print MSG "\n-------------- ID list => $aCluster -------------- \n";

       for(my $bb = 0; $bb<$clustArNum; $bb++){
          my $key = $clustAr[$bb];

          print OUT "$key ";
          print MSG "$key ";
          if (defined ${$ItemCountScore}{$key}[1]){
             print OUT "${$ItemCountScore}{$key}[1]\n";
             print MSG "${$ItemCountScore}{$key}[1]\n";
          }

       }

          my $tmp = $Clust[$aa];
          $tmp =~ s/\,/ /g;
          $tmp =~ s/\+/ +/g;
          print OUT "$tmp\n";
          print MSG "$tmp\n";
       close(OUT);
       close(MSG);

    }elsif($aCluster =~ /^[a-zA-Z0-9_]+\+([a-zA-Z0-9_]+.+)$/){
       my $clustItem = $1;
       $clustItem =~ s/\+//g;
       $clustItem =~ s/\,/-/g;
       push (@profile_name, $clustItem);

       my @clustAr = split(/\-/,$clustItem);
       my $clustArNum = @clustAr;

       open(OUT, "> /tmp/ScoreMatrixTool/$time0/$clustItem.txt");
       open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
       print MSG "\n-------------- ID list => $clustItem -------------- \n";

       for(my $bb = 0; $bb<$clustArNum; $bb++){
          my $key = $clustAr[$bb];

          print OUT $key."  :  ";
          print OUT "${$ItemCountScore}{$key}[1]\n";

          print MSG $key."  :  ";
          print MSG "${$ItemCountScore}{$key}[1]\n";
       }

          print OUT "$Clust[$aa]\n";
          print MSG "$Clust[$aa]\n";
       close(OUT);
       close(MSG);
    }elsif ($aCluster =~ /^([A-Z0-9a-z_]+)$/){
       my $first = $1;
       push (@profile_name, $first);

       if ($aCluster ne "G99"){
          open(OUT, "> /tmp/ScoreMatrixTool/$time0/$first.txt");
          open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
          print MSG "\n-------------- ID list => $first -------------- \n";

             print OUT $first."  :  ";
             print OUT "${$ItemCountScore}{$first}[1]\n";
             print OUT "$Clust[$aa]\n";
             print MSG $first."  :  ";
             print MSG "${$ItemCountScore}{$first}[1]\n";
             print MSG "$Clust[$aa]\n";
          close(OUT);
          close(MSG);
       }
    }
 }
 return (@profile_name);
}







#################################################################
#                           Get_matF                            #
#                                                               #
#  input: PKCF file name(s), $matF (blank matrix. 1000*1000)	#
#	   and $time0						#
#  output: @matF, @allLinkAr and $linkTotal                     #
#    $matF -> multidimensional hash				#
#            key: (index# of @allLinkAr[index# of @allLinkAr])  #
#            value: the number of aligned                       #
#    @allLinkAr -> complete range of links in a block           #
#    $linkTotal -> the total number of alignment in all blocks  #
#################################################################

sub Get_matF
{
 ($profile_name, $matF, $time0) = @_;

 my %nodeHash;			#key: node number => value: @tmpNode (else of a NODE line, split with \s)
 my @nodeAr = ();		#sorted keys of %nodeHash
 my @linkAr = ();		#links which are aligned (index numbers of allLinkAr)
 my @allLinkAr = ();		#complete range of links
 my $linkTotal = 0;		#count up the number of alignments
 my %missing_entries = ();	#key: missing data (link) => value: entry name(s)
 my $tempentry;
 my @tempentry_ar;

 foreach my $name (@{$profile_name}){
    $name =~ s/\,/-/g;
    $mode = 0;
    if($name=~ /-/){
       $pkcf = "/tmp/ScoreMatrixTool/$time0/pkcf-$name.txt";
       open (PKCF, "<$pkcf");
       while(<PKCF>){
          $line = $_;
	  if($line =~ /ENTRY\s+(\S+)\s+Glycan/i){
	     @tempentry_ar = split(/-/,$1);	#each Entry Name of a cluster
          }elsif($line =~ /NODE/){
             $mode = 1;
          }elsif($line =~ /EDGE/){
             $mode = 2;
          }elsif($line =~ /\/\/\//){
             $mode = 3;
          }

          if($mode == 1 && $line !~ /(ENTRY|NODE|=0|=\-)/){	#0:Gap and -:End
             chomp($line);
             $line =~ s/^\s+//g;
             my @tmpNode = split(/\s+/,$line);
	     pop(@tmpNode);			#eliminate Y coordinate
	     pop(@tmpNode);			#eliminate X coordinate
             my $key = shift(@tmpNode);		#use a Node Number as a key
             $nodeHash{$key} = \@tmpNode;
          }elsif($mode == 2 && $line =~ /EDGE/){
             my @tmp_nodeAr = keys(%nodeHash);		#Node Numbers
             @nodeAr = sort {$a <=> $b} @tmp_nodeAr;	#Sorted Node Numbers
             $count = -1;
          }elsif($mode == 2 && $line !~ /EDGE/){
             chomp($line);
             $line =~ s/^\s+//g;
             $tmp_index = -1;
             if($line =~ /^\d+\s+(\d+)\-(\d+):{0,1}([ab]\d){0,1}\s+\d+\-\d+:{0,1}(\d+){0,1}/){
                $nodeCnum1 = $1;		#Node Number
                $nodeCnum2 = $2;		#inside Node Number
                $edgeC = $3;			#anomer (a or b) and non-reducing bond
                $edgeP = $4;			#reducing bond
                my $check_item = 0;
                my $size_allLinkAr = @allLinkAr;	#@allLinkAr: complete range of links
                foreach my $item ( @nodeAr ){		#Sorted Node Number
                   if($item == $nodeCnum1){
                      $check_item = 1;
                      my $nodeC = $nodeHash{$item}[$nodeCnum2-1];	#i.g. "1=GlcNAc"
                      $nodeC =~ s/^\d+=//g;				#i.g. $nodeC = "GlcNAc"
                      $link = $nodeC.":".$edgeC."-".$edgeP;		#i.g. $link = "GlcNAc:b1-4"
		      


		      ###########################################################################
		      # checking missing data (possibly)					#
		      #    If there is/are missing link data, 					#
		      #    make a list of missing link information and there Entry Name(s) 	#
		      #   									#
		      #    A list: %missing_entries 						#
		      #    	   key: a missing link						#
		      #    	   value: an array (a list of Entry Name(s))			#
		      ###########################################################################
		      $tempentry = $tempentry_ar[$nodeCnum2-1];		#Entry Name (i.g. "G00158")
		      if(($edgeC =~ /^\d$/ && $edgeP =~ /\d/) || ($edgeC =~ /^[ab]$/ && $edgeP =~ /\d/) || ($edgeC =~ /^[ab]\d$/ && $edgeP eq "")){
									#i.g. "1-4" or "b-4" or "b1-"
			 my @linkarray = keys(%missing_entries);
			 my $linkcount = @linkarray;
			 if($linkcount == 0){
		  	    my @addarray = ($tempentry);			#(i.g. $tempentry = "G00158")
			    $missing_entries{$link} = @addarray;		#Hash(%missing_entries): key=$link(missing link) and value=@addarray
			 }else{
			    foreach my $linkname (@linkarray){
			       if($linkname eq $link){
	 		          my @array = @{$$missing_entries{$linkname}};
				  my $outputcheck = -1;
				  foreach my $entname (@array){
				     if($entname eq $tempentry){
					$outputcheck = 1;
				     }
				  }
				  if($outputcheck == -1){
				     push(@{$missing_entries{$link}},$tempentry); 
				  }
			       }else{
		  	          my @addarray = ($tempentry);
			          @{$missing_entries{$link}} = @addarray;
			       }
			    }
			 }
		      }
			       
		      
                      if ($size_allLinkAr == 0){			#adding a new link into @allLinkAr (no overlaps)
                         push(@allLinkAr, $link);
                         $tmp_index = 0;
                      }else{
                         my $size_allLinkAr = @allLinkAr;
                         for (my $i=0; $i <= $size_allLinkAr-1; $i++ ) {
                            if ( $allLinkAr[$i] eq $link) {
                               $tmp_index = $i;
                               last;
                            }
                         }
                         if($tmp_index == -1){
                            push(@allLinkAr, $link);
                            $tmp_index = @allLinkAr-1;
                            my $size_allLinkAr = @allLinkAr;
                         }
                      }


                      if($tmp_index != -1 && $check_item == 1){	#$tmp_index: an index# of a link contained in @allLinkAr
                         if($count == -1){				#if a link has the first listed Number at the line of a Node Number (i.g. 1   1=Glc ...)
                            $count = $nodeCnum1;
                            push(@linkAr, $tmp_index);
                         }elsif($nodeCnum1 == $count){			#if a link is listed in the same Node Number($nodeCnum1 = $count)
                            push(@linkAr, $tmp_index);
                         }elsif($nodeCnum1 != $count){			#if a link has the first listed Number at the line of a Node Number (i.g. 2   1=Glc ...)
                            $count = $nodeCnum1;
                            ($matF, $linkTotal) = &Count_matF($matF, \@linkAr, $linkTotal);	#This input @linkAr contains previous Node Number's link information
                            @linkAr = ();
                            push(@linkAr, $tmp_index);
                         }
                      }
                   }
                   $check_item = 0;
                }
             }
             $tmp_index = -1;
          }elsif($mode == 3 && $line =~ /\/\/\//){
             ($matF, $linkTotal) = &Count_matF($matF, \@linkAr, $linkTotal);
             @linkAr = ();
             %nodeHash = ();
	     @nodeAr = ();
          }
       }
       close(PKCF);

       open (MSG, ">> /tmp/ScoreMatrixTool/$time0/msg.txt");
       print MSG "\n\n--------------- matF ---------------\n";
       print MSG "Profile name => $name\n\n";
       my $sizelinks = @allLinkAr;
       for(my $i = 0; $i < $sizelinks; $i++){
          print MSG "$allLinkAr[$i]\n"; 
       }

       for(my $i = 0; $i < $sizelinks; $i++){
          for(my $j = 0; $j <= $i ; $j++){
             print MSG "$matF[$j][$i], "; 
          }
          print MSG "\n";
       }
       close(MSG);

    }
 }
 return (\@{$matF}, \@allLinkAr, $linkTotal, \%missing_entries);
}


#########################################
#               Count_matF              #
#  count up the time of alignment       #
#########################################

sub Count_matF
{
 ($matF, $linkAr, $linkTotal) = @_;
 my $linkAr_size = @{$linkAr};

 for(my $i=0; $i<$linkAr_size-1; $i++){
    my $index_I = @{$linkAr}[$i];

    for(my $j=$i+1; $j<$linkAr_size; $j++){
       my $index_J = @{$linkAr}[$j];
       $linkTotal += 1;

       if($index_I > $index_J){
          if (@{$matF[$index_J]}[$index_I] eq ''){
             @{$matF[$index_J]}[$index_I] = 1;
          }else{
             @{$matF[$index_J]}[$index_I] += 1;
          }
       }else{
          if (@{$matF[$index_I]}[$index_J] eq ''){
             @{$matF[$index_I]}[$index_J] = 1;
          }else{
             @{$matF[$index_I]}[$index_J] += 1;
          }
       }
    }
 }
 return(\@{$matF}, $linkTotal);
}



#################################################
#		    Copy_Arrays	 		#
#						#
#   make a copy of multidimentional array	#
#   Copy_Arrays(\@ORIGINAL ARRYA, \@Copy)	#
#################################################

sub Copy_Arrays(\@\@\@){
  my ( $orig, $copy ) = @_;
  @{$copy} = ();
  foreach ( @{$orig} ){ push( @{$copy}, [ @$_ ] ); }
}



#################
# Get post data #
#################
sub get_post_data
{
   $formdata = new CGI;
   $formdata -> param;  #error processing
   die($formdata -> cgi_error) if ($formdata -> cgi_error);

   $maxsize = "1000";
   my $size = (stat($formdata))[7];
   if($size > $maxsize * 1024){
       print "The input size is too large. Max $maxsize kB";
   }

   my $matrixname = $formdata->param('matrixname');   #Get matrixname 
   my $type = $formdata->param('type');         #Get type KCF or Gname
   my $glycan = $formdata->param('GlycanData');         #Get glycan datas
   my $threshold = $formdata->param('Threshold');        # cluster dividing length
   my $gap = $formdata->param('gap');        #MCAW parameter
   my $mono = $formdata->param('mono');        #MCAW parameter
   my $anomer = $formdata->param('anomer');        #MCAW parameter
   my $nred = $formdata->param('nred');        #MCAW parameter
   my $red = $formdata->param('red');        #MCAW parameter

   my $fileread  = $formdata->param('file');

   if($glycan =~ /\r/){
      $glycan =~ s/\r//g;
   }
   while($glycan =~ /\n\n/){
      $glycan =~ s/\n\n/\n/g;
   }
   $glycan =~ s/^\n//g;

   ####################
   # Error processing #
   ####################
   $error ='';

   if($matrixname eq ''){
       $error .= "Please input Matrix Name.\n<BR>";
   }elsif($matrixname =~ / /){
       $error .= "Can not use space(' ') for 'Matrix Name'.\n<BR>";
   }
   if($glycan eq '' && $fileread eq ''){
       $error .= "Please input Glycan Data.\n<BR>";
   }
   if($threshold eq '' || $threshold !~ /^\d+$/ ){
       $error .= "Please input Threshold.\n<BR>";
   }

   if($error ne ''){
       &printHeader("ERROR: $error");
       exit;
   }

   #################
   # fileread mode #
   #################
   if($glycan eq ''){
      my $fh = $formdata->upload('file') or die(qq(Invalid file handle returned.)); # Get $fh
      my $file_name = ($fileread =~ /([^\\\/:]+)$/) ? $1 : 'uploaded.bin';
print "file_name = $file_name<br>";
      open(OUT, ">$file_name") or die(qq(Can't open "$file_name".));
      my $buffer;
      while (read($fh, $buffer, 1024)) { # Read from $fh insted of $file
         $buf =~ s/</&lt;/g;
         $buf =~ s/>/&gt;/g;
         $glycan .= $buffer;
      }
      close(OUT);

      if($glycan eq ''){
         &printHeader("ERROR: File is empty.");
         exit;
      }
   }

   #########################
   # Get KCF from GlycanID #
   #########################
   ##############################################################
   # Rearrangement (case conversion) of Node name from GlycanID #
   ##############################################################
   if($type == 1){
      my @glycanIDs = split(/\n/ ,$glycan);
      chomp(@glycanIDs);
      $glycan = '';
      foreach my $gid (@glycanIDs){
         $gid =~ s/^\s+//g;
         $gid =~ s/\s+/ /g;
         my @glycanIDSpace = split(/ / ,$gid);
         $getkcf = &Get_EditedKCF_From_Glycan_Name($dbh,$glycanIDSpace[0]);
         $getkcf =~ s/^\n//g;

         my $weight = 1;
         if(@glycanIDSpace == 2){
            $weight = $glycanIDSpace[1];    # weight num
         }elsif(@glycanIDSpace > 2){
            &printHeader("ERROR: glycanID is incorrect -> $gid\n");
            exit;
         }
         if($getkcf eq ''){
            &printHeader("ERROR: GlycanID is incorrect -> $gid\n");
            exit;
         }
         for(my $i = 0; $i < $weight ;$i++){
            $glycan .= $getkcf;
         }
      }
   }else{
      my @glycans = split(/\n/,$glycan);
      chomp(@glycans);
      $glycan = '';
      my $mode = 0;
      my $count_id = 1;
      foreach my $gid (@glycans){
         if($gid =~ /ENTRY\s+Glycan/i) {
            $gid = "ENTRY       E".$count_id."       Glycan";
	    $count_id += 1;
         }elsif ($gid =~ /ENTRY\s+([^A-Za-z0-9]+)\s+Glycan/i) {
            $gid =~ s/[^A-Za-z0-9]/_/g;
         }elsif ($gid =~ /NODE/){
	    $mode = 1;
         }elsif ($gid =~ /EDGE/){
            $mode = 0;
         }
         
         if($mode == 1 && $gid !~ /NODE/){
            $gid = lc $gid;
         }
	 $gid .= "\n";
         $glycan .= $gid;
      }
   }

   my %mapping = &Get_Sugar_Mapping4($dbh);
   my %tmap;
   while(($tk,$tv)=each(%mapping)){
      $tk =~ tr/A-Z/a-z/;
      $tv =~ tr/A-Z/a-z/;
      $tmap{$tk} = $tv;
   }

   my $node_map = 0;
   my $glycan2 = '';
   my @tmp_glycan = split(/\n/,$glycan);
   for my $gLINE (@tmp_glycan){
       if ($gLINE =~ /NODE/) {
           $node_map = 1;
       }elsif ($gLINE =~ /EDGE/) {
           $node_map = 0;
       }

       if ($node_map == 1 && $gLINE =~ /(\d+)\s+(.+)\s+(-?\d+)\s+(-?\d+)$/){
          my $num = $1;
          my $gname = $2;
	  $gname =~ tr/A-Z/a-z/;
          my $x = $3;
          my $y = $4;

	  my $map_gname = $tmap{$gname};
          if ($map_gname ne ''){
             $gLINE = "     ".$num."     ".$map_gname."     ".$x."     ".$y;
	  }else{
             $gLINE = "     ".$num."     ".$gname."     ".$x."     ".$y;
	  }
       }
       $glycan2 .= $gLINE."\n";
   }

   $threshold =~ s/\s+//g;

   my @postdatas = ($matrixname,$glycan2,$threshold,$gap,$mono,$anomer,$nred,$red);
   @postdatas;
}




###################
# Create KCF file #
###################
sub create_sourceKCF_file {
   if(!open(DIR1, "/tmp/ScoreMatrixTool/$time0/inputKCF.txt" )) {
      open(OUT, "> /tmp/ScoreMatrixTool/$time0/inputKCF.txt");
         print OUT "$GLYCAN\n";
      close(OUT);
   }else{
      &printHeader("ERROR: TIME ERROR.\n");
      close(DIR1);
      exit;
   }

   if(!open(DIR, "/var/www/userdata/$id/ScoreMatrixTool/inputKCF$time0.kcf" )) {
      open(OUT, "> /var/www/userdata/$id/ScoreMatrixTool/inputKCF$time0.kcf");
         print OUT "$GLYCAN\n";
      close(OUT);
   }else{
      &printHeader("ERROR: TIME ERROR.\n");
      close(DIR);
      exit;
   }
}





#################################
#	   printHeader		#
#################################
sub printHeader()  # header for error output
{
print <<EOF;
 <table border=0 cellpadding=0 cellspacing=0>
 <tr valign="top">
 <td width=200>
 <table border=0 cellspacing=0 cellpadding=0>
    <h4 style="text-align : center;" align="center">
       <a href="/index.html" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Home</a>
    </h4>

    <h4 style="text-align : center;" align="center">
       <a href="/help2.html" target=_blank onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Help</a>
    </h4>

    <h4 style="text-align : center;" align="center">
       <a href="/cgi-bin/tools/Bug/BugReportForm.pl?tool= Score Matrix Tool" onMouseover="change(this,'#9966FF','#000033')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a>
    </h4>

 </table>
 </td>
 <td align = "center" width = 800><Font size = 4><B>
EOF

 print shift @_; #output error messages.
 print "<B></font></td></tr></table></body></html>";

}

#=============================#
#user system of administration#
#=============================#
sub Make_Dir{
        my $SAVE_DIR = shift @_;
        if(!-d $SAVE_DIR){
                mkdir ($SAVE_DIR) || die "failed: $!";
        }
}





#########################################
#	   Get Missing Data		#
#					#
#  find missing data from result	# 
#########################################
sub GetMissing
{
 my ($link1, $link2, $link3, $link4) = @_;
 my $check = -1;

 if($link2 =~ /^(.+):([ab]?)(\d)?-(\d)?/){
    my $node1 = $1;
    my $ab1 = $2;
    my $edge11 = $3;
    my $edge12 = $4;

    if($link1 eq $link3 && $link1 =~ /^(.+):[ab]\d-\d$/){
       if($link4 =~ /^(.+):([ab]?)(\d)?-(\d)?$/){
          my $node2 = $1;
          my $ab2 = $2;
          my $edge21 = $3;
          my $edge22 = $4;

          if($node1 eq $node2){
	     if($ab1 eq $ab2 && $edge11 eq $edge21){
	        if(($edge12 eq '' && $edge22 ne '') or ($edge12 ne '' && $edge22 eq '')){
	           $check = 1;
		   return ($check);
	        }

	     }elsif($ab1 eq $ab2 && $edge12 eq $edge22){
	        if(($dege11 eq '' && $edge21 ne '' ) || ($dege11 ne '' && $edge21 eq '')){
	           $check = 1;
		   return ($check);
	        }

             }elsif($edge11 eq $edge21 && $edge12 eq $edge22){
	        if(($ab1 eq '' && $ab2 ne '') || ($ab1 ne '' && $ab2 eq '')){
	           $check = 1;
		   return ($check);
	        }
	     }
          }
       }

    }elsif($link1 eq $link4 && $link1 =~ /^(.+):[ab]\d-\d$/){
       if($link3 =~ /^(.+):([ab]?)(\d)?-(\d)?$/){
          my $node2 = $1;
	  my $ab2 = $2;
	  my $edge21 = $3;
	  my $edge22 = $4;

          if($node1 eq $node2){
	     if($ab1 eq $ab2 && $edge11 eq $edge21){
	        if(($edge12 eq '' && $edge22 ne '') || ($edge12 ne '' && $edge22 eq '')){
	           $check = 1;
		   return ($check);
	        }

	     }elsif($ab1 eq $ab2 && $edge12 eq $edge22){
	        if(($dege11 eq '' && $edge21 ne '') || ($dege11 ne '' && $edge21 eq '')){
	           $check = 1;
		   return ($check);
	        }

             }elsif($edge11 eq $edge21 && $edge12 eq $edge22){
	        if(($ab1 eq '' && $ab2 ne '') || ($ab1 ne '' && $ab2 eq '')){
	           $check = 1;
		   return ($check);
	        }
	     }
          }
       }
    }

 }

 if($link1 =~ /^(.+):([ab]?)(\d)?-(\d)?/){
    my $node1 = $1;
    my $ab1 = $2;
    my $edge11 = $3;
    my $edge12 = $4;

    if($link2 eq $link3 && $link2 =~ /^(.+):[ab]\d-\d$/){
       if($link4 =~ /^(.+):([ab]?)(\d)?-(\d)?$/){
          my $node2 = $1;
          my $ab2 = $2;
          my $edge21 = $3;
          my $edge22 = $4;

          if($node1 eq $node2){
	     if($ab1 eq $ab2 && $edge11 eq $edge21){
	        if(($edge12 eq '' && $edge22 ne '') || ($edge12 ne '' && $edge22 eq '')){
	           $check = 1;
		   return ($check);
	        }

	     }elsif($ab1 eq $ab2 && $edge12 eq $edge22){
	        if(($dege11 eq '' && $edge21 ne '') || ($dege11 ne '' && $edge21 eq '')){
	           $check = 1;
		   return ($check);
	        }

             }elsif($edge11 eq $edge21 && $edge12 eq $edge22){
	        if(($ab1 eq '' && $ab2 ne '') || ($ab1 ne '' && $ab2 eq '')){
	           $check = 1;
		   return ($check);
	        }
	     }
          }
       }

    }elsif($link2 eq $link4 && $link2 =~ /^(.+):[ab]\d-\d$/){
       if($link3 =~ /^(.+):([ab]?)(\d)?-(\d)?$/){
          my $node2 = $1;
	  my $ab2 = $2;
	  my $edge21 = $3;
	  my $edge22 = $4;

          if($node1 eq $node2){
	     if($ab1 eq $ab2 && $edge11 eq $edge21){
	        if(($edge12 eq '' && $edge22 ne '') || ($edge12 ne '' && $edge22 eq '')){
	           $check = 1;
		   return ($check);
	        }

	     }elsif($ab1 eq $ab2 && $edge12 eq $edge22){
	        if(($dege11 eq '' && $edge21 ne '') || ($dege11 ne '' && $edge21 eq '')){
	           $check = 1;
		   return ($check);
	        }

             }elsif($edge11 eq $edge21 && $edge12 eq $edge22){
	        if(($ab1 eq '' && $ab2 ne '') || ($ab1 ne '' && $ab2 eq '')){
	           $check = 1;
		   return ($check);
	        }
	     }
          }
       }
    }
 }
}


#################################
#           (Aindex)            # 
#################################
sub aindex (\@$;$) 
{
  my ($aref, $val, $pos) = @_;
  for ($pos ||= 0; $pos < @$aref; $pos++) {
    return $pos if $aref->[$pos] eq $val;
  }
  return -1;
}
